#!/usr/bin/env python

### INCLUDED MODULE 
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,DATE,ForeignKey,Float
from os import path
from sqlalchemy import *

import sys
import datetime
import time
import re
import json
import subprocess
import logging
import os
import gzip

class pmbDB:
    'Common pmbDB'
    
    def __init__(self, filename):
        self.filename     = filename

        if path.isfile(filename):
            print "Database file %s already exists" % filename
            return
    
        self.db = create_engine('sqlite:///'+self.filename)
        self.db.echo = False  
        self.metadata = MetaData(self.db)

        self.job = Table('job', self.metadata,
                         Column('jid'             ,  Integer, primary_key=True),   
                         Column('name'            ,  String(60)  ),                
                         Column('step'            ,  String(20)  ),                
                         Column('platform'        ,  String(30)  ),                
                         Column('release'         ,  String(30)  ),                
                         Column('date'            ,  DATE 	      ),                          
                         Column('nightly'         ,  String(10)  ),                
                         Column('exitcode'        ,  Integer     ),                
                         Column('vmem_peak'       ,  Integer     ),                
                         Column('vmem_mean'       ,  Integer     ),                
                         Column('rss_mean'        ,  Integer     ),                
                         Column('jobcfg_walltime' ,  Integer     ),                
                         Column('jobstart'        ,  Integer     ),                
                         Column('cpu_bmips'       ,  Integer     ),                
                         Column('cpu_res'         ,  Integer     ),                
                         Column('cpu_model'       ,  String(60)  ),                
                         Column('malloc'          ,  Integer     ),                
                         Column('cost_perevt'     ,  Integer     ),                
                         Column('cost_onceperjob' ,  Integer     ))                                                           
        

        self.job.create()
    
        self.component = Table('component', self.metadata,
                               Column('name',     String(80)  ),
                               Column('domain',   String(40)  ),
                               Column('stage',    String(10)  ),
                               Column('n',        Integer     ),
                               Column('cpu',      Integer     ),
                               Column('maxcpu',   Integer     ),
                               Column('wall',     Integer     ),
                               Column('vmem',     Integer     ),
                               Column('maxvmem',  String(10)  ),
                               Column('malloc',   Integer     ),
                               Column('maxmalloc',String(10)  ),
                               Column('jid', Integer, ForeignKey('job.jid'))
                           )
        self.component.create()
    
        self.container = Table('container', self.metadata,
                               Column('name',         String(200)  ),
                               Column('domain',       String(40)   ),
                               Column('items',        Integer      ),
                               Column('compression',  Float        ),
                               Column('sizeperevt',   Float        ),
                               Column('disksize',     Float        ),
                               Column('jid', Integer, ForeignKey('job.jid'))
                           )
        self.container.create()


    def merge_db(self,fileToMerge):
        
        import sqlite3
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()


        cmd = 'attach "{0}" as toMerge'.format(fileToMerge)
        print cmd
        c.execute(cmd)
        
        cmd = "begin"
        c.execute(cmd)
        
        cmd = "select job.jid,job.release,job.nightly,job.step from toMerge.job"                               
        c.execute(cmd)
        
        all_rows = c.fetchall()
        print('1):', all_rows)
    
        cmd = "insert into job select * from toMerge.job"                               
        try:
            c.execute(cmd)
        except:
            print "pass :-("
        pass
        
        cmd = "insert into component select * from toMerge.component"                               
        c.execute(cmd)
        cmd = "insert into container select * from toMerge.container"                               
        c.execute(cmd)
        cmd = "detach database toMerge"                                                             
        c.execute(cmd)
        
        return

    def merge_db_files(self,filesToMergeList):
        
        import sqlite3
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()

        for fileToMerge in filesToMergeList:

            cmd = 'attach "{0}" as toMerge'.format(fileToMerge)
            print cmd
            c.execute(cmd)
        
            cmd = "begin"
            c.execute(cmd)
        
            cmd = "select job.jid,job.release,job.nightly,job.step from toMerge.job"                               
            c.execute(cmd)
        
            all_rows = c.fetchall()
            print('1):', all_rows)
    
            cmd = "insert into job select * from toMerge.job"                               
            try:
                c.execute(cmd)
            except:
                print "pass :-("
            pass
        
            cmd = "insert into component select * from toMerge.component"                               
            c.execute(cmd)
            cmd = "insert into container select * from toMerge.container"                               
            c.execute(cmd)
            cmd = "detach database toMerge"                                                             
            c.execute(cmd)
        
        return


    def addEntry(self,release,platform,nightly,jobname,jobpath):

        path     = jobpath
    
        import glob
        StepLogFiles  = glob.glob(path+'/log.*')

        for logFile in StepLogFiles:
            step = logFile.split('log.')[1]

        long_name = jobname+"_"+platform+"_"+release+"_"+nightly    

        import calendar
        abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}

        for logFile in StepLogFiles:
            step = logFile.split('log.')[1]

            print logFile

            try:
                infile = open(logFile)
            except:
                continue

            data = {}
            data["name"]     = jobname
            data["platform"] = platform
            data["nightly"]  = nightly
            data["release"]  = release
            data["step"]     = step
        
            my_domain_dict = {}
            my_domain_dict['AthAlgSeq']="master"
            my_domain_dict['RecoMemoryBegin']="master"
            my_domain_dict['RecoTimerBegin']="master"
            my_domain_dict['AthMasterSeq']="master"
            my_domain_dict['AthOutSeq']="master"
            my_domain_dict['AthRegSeq']="master"
            my_domain_dict['StreamAOD']="output"
            
            exitcode=None
            timestamp=0
            #FIRST ITERATION THROUGH FILE TO INPUT JOB INFO AND GATHER DOMAIN INFORMATION
            for line in infile:
                if ' CET ' in line or ' CEST ' in line:
                    timestamp=line.split()
                    myYear    = timestamp[6]
                    myMonth   = timestamp[2]
                    myDay     = timestamp[3]
                    
                    myTime    = timestamp[4]
                    myHour    = myTime.split(':')[0]
                    myMinute  = myTime.split(':')[1]
                    mySecond  = myTime.split(':')[2]

                    myMonthNum =  abbr_to_num[myMonth]
                    dt = datetime.datetime(int(myYear), int(myMonthNum), int(myDay), int(myHour), int(myMinute), int(mySecond))
                    UTCtime = int((dt-datetime.datetime(1970,1,1)).total_seconds())
                    ISOtime = dt.isoformat()
                    data["timestamp"]=ISOtime
                    data["jid"]=UTCtime

                if '-- built on' in line:
                    print line
                    mydate = (((line.split('[')[4]).split(']')[0]).split('T')[0])
                    print mydate
                    mydateJ  =mydate.split('-')
                    mydate = datetime.date(int(mydateJ[0]),int(mydateJ[1]),int(mydateJ[2]))

                if 'PerfMon domains :' in line:
                    domains = line.split(':')[-1]
                    domains = re.sub('[\[\]\',\n"]', '', domains).split(" ")
                    domains = [x for x in domains if x is not None]
                    domains.remove('')
                    domains.remove('')

                if '::: domain [' in line and not 'WARNING' in line:
                    specific_domain = [re.sub('[\]\[\n]','',line.split(' ')[-1])]
                    nextline = next(infile)
                    myalgs = nextline 
                    myalgs = myalgs.split('algs:')[-1]
                    myalgs = re.sub('[\[\]\',"]', '', myalgs).split()
                    myalgs = [x for x in myalgs if x is not None or x is not '']
                 
                    for myalg in myalgs:
                        my_domain_dict[str(myalg)]=str(specific_domain[0])

                if 'vmem_peak' in line :
                    data["vmem_peak"]=int((line.split()[3]).split('=')[1])
                    data["vmem_mean"]=int((line.split()[4]).split('=')[1])
                    data["rss_mean"]=int((line.split()[5]).split('=')[1])
                    
                if 'jobcfg_walltime' in line :
                    data["jobcfg_walltime"]=int((line.split()[3]).split('=')[1])
                    data["jobstart"]       =(line.split()[4]).split('=')[1]
                    
                if 'cpu_bmips' in line :
                    data["cpu_bmips"]=int((line.split()[3]).split('=')[1])
                    data["cpu_res"]=int((line.split()[4]).split('=')[1])

                if 'cpu_model' in line :
                    data["cpu_model"]=(line.split()[3]).split('=')[1]
                    
                if 'pycintex_vmemfix' in line :
                    data["malloc"]=(line.split()[3]).split('=')[1]

                if 'pmonsd_cost' in line :
                    data["pmonsd_cost_onceperjob"]=int((line.split()[3]).split('=')[1])
                    data["pmonsd_cost_perevt"]    =int((line.split()[4]).split('=')[1])

                if 'INFO leaving with code' in line:
                    exitcode = int(line.split()[6].split(':')[0])
                    data["exitcode"]=exitcode
                
            print "exitcode ", exitcode
            if exitcode != 0 :
                continue

            ins = self.job.insert()
            result = self.db.execute(ins,
                                       jid             =  data["jid"]                   ,
                                       name            =  data["name"]                  ,
                                       platform        =  data["platform"]              ,
                                       release         =  data["release"]               ,
                                       step            =  data["step"]                  ,
                                       date            =  mydate                        ,
                                       nightly         =  data["nightly"]               ,
                                       exitcode        =  data["exitcode"]              ,  
                                       vmem_peak       =  data["vmem_peak"]             ,  
                                       vmem_mean       =  data["vmem_mean"]             ,
                                       rss_mean        =  data["rss_mean"]              ,
                                       jobcfg_walltime =  data["jobcfg_walltime"]       ,
                                       jobstart        =  data["jobstart"]              ,
                                       cpu_bmips       =  data["cpu_bmips"]             ,
                                       cpu_res         =  data["cpu_res"]               ,
                                       cpu_model       =  data["cpu_model"]             ,
                                       malloc          =  data["malloc"]                ,
                                       cost_perevt     =  data["pmonsd_cost_perevt"]    ,
                                       cost_onceperjob =  data["pmonsd_cost_onceperjob"] )                           
        
            components = []

            data['cbk']={}
            data['ini']={}
            data['1st']={}
            data['evt']={}
            data['fin']={}
            data['dso']={}
            data['---']={}
            data['preLoadProxy']={}
        
            infile = open(logFile)                    
            for line in infile:
                if '[evt]' in line and not '[total_for' in line and not 'collapsed' in line:
                    pmon    =line.split()
                    stage   =str(pmon[2])
                    nevts   =int(pmon[3])
                    cpu     =int(pmon[4])
                    maxcpu  =int(str(pmon[5]).split("@")[0])
                    wall    =None
                    vmem    =int(pmon[6])
                    maxvmem =str(pmon[7])
                    mlc     =int(pmon[8])
                    maxmlc  =str(pmon[9])
                    cname   =str(pmon[10])
                    try:
                        domain=str(my_domain_dict[cname])
                    except Exception:
                        domain=None
                
                    data['evt'].update(
                        { cname : {   "domain" :  domain       ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc    }
                      })


                    new_component = { "jid"  :  data["jid"],
                                      "name"   :  cname         ,
                                      "step"   :  step         ,
                                      "domain" :  domain       ,
                                      "stage"  :  "evt"        ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc       ,
                                      "step"   :  step         }
                            
                    components.append(new_component)
                            
                    ins = self.component.insert()
                    self.db.execute(ins,
                                      jid             =  data["jid"]      ,
                                      name            =  cname            ,
                                      step            =  step             ,
                                      domain          =  domain           ,
                                      stage           =  stage            ,
                                      n               =  nevts            ,
                                      cpu             =  cpu              ,
                                      maxcpu          =  maxcpu           ,
                                      wall            =  wall             ,
                                      vmem            =  vmem             ,
                                      maxvmem         =  maxvmem          ,
                                      malloc          =  mlc              ,
                                      maxmalloc       =  maxmlc           )
                            
                            
                elif 'PMonSD' in line and ('[ini]' in line or '[1st]' in line or '[cbk]' in line or '[dso]' in line or '[fin]' in line or '[preLoadProxy]' in line or '[evt]' in line or '[---]' in line) and ('vmem_peak' not in line and
                'jobstart' not in line and
                'bmips' not in line and
                'cpu_model' not in line and
                'vmemfix' not in line and
                'pmonsd_cost_perevt' not in line):
                
                    pmon =line.split()

                    stage=str(pmon[2])
                    nevts=int(pmon[3])
                    cpu  =pmon[4]
                    vmem =pmon[5]
                    mlc  =pmon[6]
                
                    wall            =  None
                    maxcpu          =  None         
                    maxvmem         =  None          
                    maxmlc          =  None           
                    cname =str(pmon[7])

                
                    if '[---]' in line:
                        stage=str(pmon[2])
                        nevts=int(pmon[3])
                        cpu  =pmon[4]
                        wall =pmon[5]
                        vmem =pmon[6]
                        mlc  =pmon[7]
                        cname =pmon[8]

                        if cpu == '-':
                            cpu=None
                        else:
                            cpu=int(cpu)
                    
                        if wall == '-':
                            wall=None
                        else:
                            wall=int(wall)
                    
                        if vmem == '-':
                            vmem=None
                        else:
                            vmem=int(vmem)
                    
                        if mlc == '-':
                            mlc=None
                        else:
                            mlc=int(mlc)
                    
                    else:
                        if '[dso]' in line:
                            cname = cname.split('/')[-1]
                            cname= cname.replace('.','_').split('/')[-1]

                        if '[cbk]' in line:
                            cname = cname.split('[0x')[0]

                        if '[ini]' in line or '[fin]' in line:
                            cname = cname.split('ToolSvc.')[-1]

                        
                        cpu  =int(cpu)
                        vmem =int(vmem)
                        mlc  =int(mlc)
                    
                    
                    cleanstage=re.sub('[\]\[]', '',stage)
                    data[cleanstage].update(
                        { cname : {   "domain" :  None         ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc    }
                      })

                    
                    
                    new_component = { "jid"    :  data["jid"]    ,
                                      "name"   :  cname        ,
                                      "step"   :  step         ,
                                      "domain" :  ""           ,
                                      "stage"  :  stage        ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc    ,
                                      "step"   :  step         }
                            
                    components.append(new_component)
                            
                    ins = self.component.insert()
                    self.db.execute(ins,
                                      jid             =  data["jid"]      ,
                                      name            =  cname            ,
                                      step            =  step             ,
                                      domain          =  ""               ,
                                      stage           =  stage            ,
                                      n               =  nevts            ,
                                      cpu             =  cpu              ,
                                      maxcpu          =  maxcpu           ,
                                      wall            =  wall             ,
                                      vmem            =  vmem             ,
                                      maxvmem         =  maxvmem          ,
                                      malloc          =  mlc              ,
                                      maxmalloc       =  maxmlc           )
                        
                        
            output_file_type = str(logFile.split('to')[-1])
            print output_file_type
            if output_file_type == "ALL":
                output_file_type = 'AOD'
            output_file_content=path+'/my'+output_file_type+'.pool.root.checkfile.txt'
            print output_file_content
            data[output_file_type]={}
            if os.path.exists(output_file_content):
                f=open(output_file_content,'r')
                for line in f:
                    items_in_line=line.split()
                    if 'kb' in line :
                        if len(line.split()) > 8 :
                            MemSize       = items_in_line[0]
                            DiskSize      = items_in_line[2]
                            SizePerEvt    = items_in_line[4]
                            Compression   = items_in_line[6]  
                            Items         = items_in_line[7]  
                            ContainerName = items_in_line[8]
                            Type = ""
                            if len(line.split()) > 10 :
                                Type          = re.sub('[\[\]]', '',items_in_line[10])
                    
                                ins = self.container.insert()
                                self.db.execute(ins,
                                                  jid             =  data["jid"]      ,
                                                  name            =  ContainerName    ,
                                                  domain          =  Type             ,
                                                  items           =  Items            ,
                                                  compression     =  Compression      ,
                                                  sizeperevt      =  SizePerEvt       ,
                                                  disksize        =  DiskSize         ,
                                                  memsize         =  MemSize          )
                                
                                data[output_file_type].update(
                                    { ContainerName : {   "domain"      :  Type         ,
                                                          "items"      :  int(Items)        ,
                                                          "compression":  float(Compression)  ,
                                                          "sizeperevt" :  float(SizePerEvt)    ,
                                                          "disksize"   :  float(DiskSize)     ,
                                                          "memsize"    :  float(MemSize)      }
                                  })
                                a = { ContainerName : {   "domain"      :  Type         ,
                                                          "items"      :  int(Items)        ,
                                                          "compression":  float(Compression)  ,
                                                          "sizeperevt" :  float(SizePerEvt)    ,
                                                          "disksize"   :  float(DiskSize)     ,
                                                          "memsize"    :  float(MemSize)      }}
                            elif len(line.split()) == 4 :
                                DiskSize     = float(items_in_line[0])
                                Fraction     = float(items_in_line[2])
                                CategoryName = str(items_in_line[3])
                                print "limo ",line
                                data[output_file_type].update(
                                    {CategoryName   :  {
                                        "fraction"      :  Fraction         ,
                                        "disksize"      :  DiskSize         ,
                                        "compression":  float(Compression)  
                                    }
                                 }
                                )
        
            json_filename = path+'/'+long_name+'_'+step+'.json'
            with open(json_filename, 'w') as fp:
                json.dump(data,fp)

        return

    def addEntryNoJSON(self,release,platform,nightly,jobname,jobpath):

        path     = jobpath
    
        import glob
        StepLogFiles  = glob.glob(path+'/log.*')

        for logFile in StepLogFiles:
            step = logFile.split('log.')[1]

        long_name = jobname+"_"+platform+"_"+release+"_"+nightly    

        import calendar
        abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}

        for logFile in StepLogFiles:
            step = logFile.split('log.')[1]

            print logFile

            try:
                infile = open(logFile)
            except:
                continue

            data = {}
            data["name"]     = jobname
            data["platform"] = platform
            data["nightly"]  = nightly
            data["release"]  = release
            data["step"]     = step
        
            my_domain_dict = {}
            my_domain_dict['AthAlgSeq']="master"
            my_domain_dict['RecoMemoryBegin']="master"
            my_domain_dict['RecoTimerBegin']="master"
            my_domain_dict['AthMasterSeq']="master"
            my_domain_dict['AthOutSeq']="master"
            my_domain_dict['AthRegSeq']="master"
            my_domain_dict['StreamAOD']="output"
            
            exitcode=None
            timestamp=0
            #FIRST ITERATION THROUGH FILE TO INPUT JOB INFO AND GATHER DOMAIN INFORMATION
            for line in infile:
                if ' CET ' in line or ' CEST ' in line:
                    timestamp=line.split()
                    myYear    = timestamp[6]
                    myMonth   = timestamp[2]
                    myDay     = timestamp[3]
                    
                    myTime    = timestamp[4]
                    myHour    = myTime.split(':')[0]
                    myMinute  = myTime.split(':')[1]
                    mySecond  = myTime.split(':')[2]

                    myMonthNum =  abbr_to_num[myMonth]
                    dt = datetime.datetime(int(myYear), int(myMonthNum), int(myDay), int(myHour), int(myMinute), int(mySecond))
                    UTCtime = int((dt-datetime.datetime(1970,1,1)).total_seconds())
                    ISOtime = dt.isoformat()
                    data["timestamp"]=ISOtime
                    data["jid"]=UTCtime

                if '-- built on' in line:
                    mydate = (((line.split('[')[4]).split(']')[0]).split('T')[0])
                    mydateJ  =mydate.split('-')
                    mydate = datetime.date(int(mydateJ[0]),int(mydateJ[1]),int(mydateJ[2]))

                if 'PerfMon domains :' in line:
                    domains = line.split(':')[-1]
                    domains = re.sub('[\[\]\',\n"]', '', domains).split(" ")
                    domains = [x for x in domains if x is not None]
                    domains.remove('')
                    domains.remove('')

                if '::: domain [' in line and not 'WARNING' in line:
                    specific_domain = [re.sub('[\]\[\n]','',line.split(' ')[-1])]
                    nextline = next(infile)
                    myalgs = nextline 
                    myalgs = myalgs.split('algs:')[-1]
                    myalgs = re.sub('[\[\]\',"]', '', myalgs).split()
                    myalgs = [x for x in myalgs if x is not None or x is not '']
                 
                    for myalg in myalgs:
                        my_domain_dict[str(myalg)]=str(specific_domain[0])

                if 'vmem_peak' in line :
                    data["vmem_peak"]=int((line.split()[3]).split('=')[1])
                    data["vmem_mean"]=int((line.split()[4]).split('=')[1])
                    data["rss_mean"]=int((line.split()[5]).split('=')[1])
                    
                if 'jobcfg_walltime' in line :
                    data["jobcfg_walltime"]=int((line.split()[3]).split('=')[1])
                    data["jobstart"]       =(line.split()[4]).split('=')[1]
                    
                if 'cpu_bmips' in line :
                    data["cpu_bmips"]=int((line.split()[3]).split('=')[1])
                    data["cpu_res"]=int((line.split()[4]).split('=')[1])

                if 'cpu_model' in line :
                    data["cpu_model"]=(line.split()[3]).split('=')[1]
                    
                if 'pycintex_vmemfix' in line :
                    data["malloc"]=(line.split()[3]).split('=')[1]

                if 'pmonsd_cost' in line :
                    data["pmonsd_cost_onceperjob"]=int((line.split()[3]).split('=')[1])
                    data["pmonsd_cost_perevt"]    =int((line.split()[4]).split('=')[1])

                if 'INFO leaving with code' in line:
                    exitcode = int(line.split()[6].split(':')[0])
                    data["exitcode"]=exitcode
                
            print "exitcode ", exitcode
            if exitcode != 0 :
                continue

            ins = self.job.insert()
            result = self.db.execute(ins,
                                       jid             =  data["jid"]                   ,
                                       name            =  data["name"]                  ,
                                       platform        =  data["platform"]              ,
                                       release         =  data["release"]               ,
                                       step            =  data["step"]                  ,
                                       date            =  mydate                        ,
                                       nightly         =  data["nightly"]               ,
                                       exitcode        =  data["exitcode"]              ,  
                                       vmem_peak       =  data["vmem_peak"]             ,  
                                       vmem_mean       =  data["vmem_mean"]             ,
                                       rss_mean        =  data["rss_mean"]              ,
                                       jobcfg_walltime =  data["jobcfg_walltime"]       ,
                                       jobstart        =  data["jobstart"]              ,
                                       cpu_bmips       =  data["cpu_bmips"]             ,
                                       cpu_res         =  data["cpu_res"]               ,
                                       cpu_model       =  data["cpu_model"]             ,
                                       malloc          =  data["malloc"]                ,
                                       cost_perevt     =  data["pmonsd_cost_perevt"]    ,
                                       cost_onceperjob =  data["pmonsd_cost_onceperjob"] )                           
        
            components = []

            data['cbk']={}
            data['ini']={}
            data['1st']={}
            data['evt']={}
            data['fin']={}
            data['dso']={}
            data['---']={}
            data['preLoadProxy']={}
        
            infile = open(logFile)                    
            for line in infile:
                if '[evt]' in line and not '[total_for' in line and not 'collapsed' in line:
                    pmon    =line.split()
                    stage   =str(pmon[2])
                    nevts   =int(pmon[3])
                    cpu     =int(pmon[4])
                    maxcpu  =int(str(pmon[5]).split("@")[0])
                    wall    =None
                    vmem    =int(pmon[6])
                    maxvmem =str(pmon[7])
                    mlc     =int(pmon[8])
                    maxmlc  =str(pmon[9])
                    cname   =str(pmon[10])
                    try:
                        domain=str(my_domain_dict[cname])
                    except Exception:
                        domain=None
                
                    data['evt'].update(
                        { cname : {   "domain" :  domain       ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc    }
                      })


                    new_component = { "jid"  :  data["jid"],
                                      "name"   :  cname         ,
                                      "step"   :  step         ,
                                      "domain" :  domain       ,
                                      "stage"  :  "evt"        ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc       ,
                                      "step"   :  step         }
                            
                    components.append(new_component)
                            
                    ins = self.component.insert()
                    self.db.execute(ins,
                                      jid             =  data["jid"]      ,
                                      name            =  cname            ,
                                      step            =  step             ,
                                      domain          =  domain           ,
                                      stage           =  stage            ,
                                      n               =  nevts            ,
                                      cpu             =  cpu              ,
                                      maxcpu          =  maxcpu           ,
                                      wall            =  wall             ,
                                      vmem            =  vmem             ,
                                      maxvmem         =  maxvmem          ,
                                      malloc          =  mlc              ,
                                      maxmalloc       =  maxmlc           )
                            
                            
                elif 'PMonSD' in line and ('[ini]' in line or '[1st]' in line or '[cbk]' in line or '[dso]' in line or '[fin]' in line or '[preLoadProxy]' in line or '[evt]' in line or '[---]' in line) and ('vmem_peak' not in line and
                'jobstart' not in line and
                'bmips' not in line and
                'cpu_model' not in line and
                'vmemfix' not in line and
                'pmonsd_cost_perevt' not in line):
                
                    pmon =line.split()

                    stage=str(pmon[2])
                    nevts=int(pmon[3])
                    cpu  =pmon[4]
                    vmem =pmon[5]
                    mlc  =pmon[6]
                
                    wall            =  None
                    maxcpu          =  None         
                    maxvmem         =  None          
                    maxmlc          =  None           
                    cname =str(pmon[7])

                
                    if '[---]' in line:
                        stage=str(pmon[2])
                        nevts=int(pmon[3])
                        cpu  =pmon[4]
                        wall =pmon[5]
                        vmem =pmon[6]
                        mlc  =pmon[7]
                        cname =pmon[8]

                        if cpu == '-':
                            cpu=None
                        else:
                            cpu=int(cpu)
                    
                        if wall == '-':
                            wall=None
                        else:
                            wall=int(wall)
                    
                        if vmem == '-':
                            vmem=None
                        else:
                            vmem=int(vmem)
                    
                        if mlc == '-':
                            mlc=None
                        else:
                            mlc=int(mlc)
                    
                    else:
                        if '[dso]' in line:
                            cname = cname.split('/')[-1]
                            cname= cname.replace('.','_').split('/')[-1]

                        if '[cbk]' in line:
                            cname = cname.split('[0x')[0]

                        if '[ini]' in line or '[fin]' in line:
                            cname = cname.split('ToolSvc.')[-1]

                        
                        cpu  =int(cpu)
                        vmem =int(vmem)
                        mlc  =int(mlc)
                    
                    
                    cleanstage=re.sub('[\]\[]', '',stage)
                    data[cleanstage].update(
                        { cname : {   "domain" :  None         ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc    }
                      })

                    
                    
                    new_component = { "jid"    :  data["jid"]    ,
                                      "name"   :  cname        ,
                                      "step"   :  step         ,
                                      "domain" :  ""           ,
                                      "stage"  :  stage        ,
                                      "n"      :  nevts        ,
                                      "cpu"    :  cpu          ,
                                      "maxcpu" :  maxcpu       ,
                                      "wall"   :  wall         ,
                                      "vmem"   :  vmem         ,
                                      "maxvmem":  maxvmem      ,
                                      "malloc" :  mlc          ,
                                      "maxmalloc" :  maxmlc    ,
                                      "step"   :  step         }
                            
                    components.append(new_component)
                            
                    ins = self.component.insert()
                    self.db.execute(ins,
                                      jid             =  data["jid"]      ,
                                      name            =  cname            ,
                                      step            =  step             ,
                                      domain          =  ""               ,
                                      stage           =  stage            ,
                                      n               =  nevts            ,
                                      cpu             =  cpu              ,
                                      maxcpu          =  maxcpu           ,
                                      wall            =  wall             ,
                                      vmem            =  vmem             ,
                                      maxvmem         =  maxvmem          ,
                                      malloc          =  mlc              ,
                                      maxmalloc       =  maxmlc           )
                        
                        
            output_file_type = str(logFile.split('to')[-1])
            print output_file_type
            if output_file_type == "ALL":
                output_file_type = 'AOD'
            output_file_content=path+'/my'+output_file_type+'.pool.root.checkfile.txt'
            print output_file_content
            data[output_file_type]={}
            if os.path.exists(output_file_content):
                f=open(output_file_content,'r')
                for line in f:
                    items_in_line=line.split()
                    if 'kb' in line :
                        if len(line.split()) > 8 :
                            MemSize       = items_in_line[0]
                            DiskSize      = items_in_line[2]
                            SizePerEvt    = items_in_line[4]
                            Compression   = items_in_line[6]  
                            Items         = items_in_line[7]  
                            ContainerName = items_in_line[8]
                            Type = ""
                            if len(line.split()) > 10 :
                                Type          = re.sub('[\[\]]', '',items_in_line[10])
                    
                                ins = self.container.insert()
                                self.db.execute(ins,
                                                  jid             =  data["jid"]      ,
                                                  name            =  ContainerName    ,
                                                  domain          =  Type             ,
                                                  items           =  Items            ,
                                                  compression     =  Compression      ,
                                                  sizeperevt      =  SizePerEvt       ,
                                                  disksize        =  DiskSize         ,
                                                  memsize         =  MemSize          )
                                
                                data[output_file_type].update(
                                    { ContainerName : {   "domain"      :  Type         ,
                                                          "items"      :  int(Items)        ,
                                                          "compression":  float(Compression)  ,
                                                          "sizeperevt" :  float(SizePerEvt)    ,
                                                          "disksize"   :  float(DiskSize)     ,
                                                          "memsize"    :  float(MemSize)      }
                                  })
                                a = { ContainerName : {   "domain"      :  Type         ,
                                                          "items"      :  int(Items)        ,
                                                          "compression":  float(Compression)  ,
                                                          "sizeperevt" :  float(SizePerEvt)    ,
                                                          "disksize"   :  float(DiskSize)     ,
                                                          "memsize"    :  float(MemSize)      }}
                            elif len(line.split()) == 4 :
                                DiskSize     = float(items_in_line[0])
                                Fraction     = float(items_in_line[2])
                                CategoryName = str(items_in_line[3])
                                print "limo ",line
                                data[output_file_type].update(
                                    {CategoryName   :  {
                                        "fraction"      :  Fraction         ,
                                        "disksize"      :  DiskSize         ,
                                        "compression":  float(Compression)  
                                    }
                                 }
                                )
        
        return

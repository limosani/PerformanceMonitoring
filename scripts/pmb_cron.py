#!/usr/bin/

# Script for running a few select jobs on a few select branches on
# specific build machine via cron. This mainly to be able to monitor
# developments in cpu usage.
#
# Several efforts are made to ensure that results are as reliable as
# possible: Job is only started if machine is not too busy, input files
# are copied locally before start, and each job is preceded by a small
# nevts=1 test job, to ensure all afs caches are set up.

import os,glob,time,datetime,sys,subprocess,shutil,subprocess,re

##############################################################################
############################# DEFINE DIRS ####################################
##############################################################################
cvmfs=True

username=os.getenv('USER')

if not os.getenv('BASEDIR'):
    print 'ERROR: $BASEDIR not defines'
    sys.exit(1)

archive_dir = '/eos/atlas/user/a/atlaspmb/archive/custom'
if not os.path.exists(archive_dir):
    print 'ERROR: '+archive_dir+' is not available'
    sys.exit(1)

nightlies_dir_cvmfs = '/cvmfs/atlas-nightlies.cern.ch/repo/sw/nightlies'
nightlies_dir_cvmfs = '/cvmfs/atlas-nightlies.cern.ch/repo/sw/'
asetup_dir='/afs/cern.ch/atlas/software/dist/AtlasSetup'
#asetup='source %s/scripts/asetup.sh'%(asetup_dir)

tmpdir_base = ''
tmp_infiles_base = ''
input_file_path = ''
free_space_checks_gb = ''

#print 'HOSTNAME: ',os.getenv('HOSTNAME')

if os.getenv('HOSTNAME')=='aibuild010.cern.ch':
    input_file_path = '/build/atlaspmb/data12_8TeV'
    tmpdir_base      = '/build/%s/custom_nightly_tests/rundirs'%username
    tmp_infiles_base = '/build/%s/custom_nightly_tests/infiles'%username
    free_space_checks_gb=[('/build1',30.0)]#todo: also check quota of target archive dir on afs
else:
    print "job must run on aibuild010.cern.ch"
    sys.exit(0)

##############################################################################
############################# DEFINE JOBS ####################################
##############################################################################

#
#A job function should return None if it doesn't want to run.
#
#Otherwise it should return (infiles,cmd) where infiles is a list of
#  tuple pairs. The former being the input file location on afs or
#  castor and the latter being the local expected location of the input file (should start with tmp_infiles_base).
#  (todo: CASTOR not supported yet)
#
#It is assumed that all output files starts with "my*"
#


def __nevts(nevts,builddate):
    if nevts=='all':
        return -1
    return nevts

####################################################################################################

def Reco_tf_cmd():
    return 'Reco_tf.py'

def __Reco_tf_hitstoaod(infile,nevts,short,amp):
    if short: nevts=1
    f=infile
    preExec=' --conditionsTag \'default:OFLCOND-RUN12-SDR-31\' --conditionsTag \'default:OFLCOND-RUN12-SDR-31\' --digiSeedOffset1 170 --digiSeedOffset2 170 --geometryVersion \'default:ATLAS-R2-2015-03-01-00\' --inputHighPtMinbiasHitsFile /build1/atlaspmb/mc/mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e3581_s2578_s2195/HITS.05608152.*.pool.root.? --inputLowPtMinbiasHitsFile /build2/atlaspmb/mc/mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e3581_s2578_s2195/HITS.05608147.*.pool.root.?  --jobNumber 1  --numberOfCavernBkg="0" --numberOfHighPtMinBias \'0.122680569785\' --numberOfLowPtMinBias \'39.8773194302\'  --outputAODFile="myAOD.pool.root" --outputESDFile="myESD.pool.root" --outputRDOFile="myRDO.pool.root"  --pileupFinalBunch \'6\' --postExec \'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' \'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools["MergeMcEventCollTool"].OnlySaveSignalTruth=True;job.StandardPileUpToolsAlg.PileUpTools["MdtDigitizationTool"].LastXing=150\' \'RDOtoRDOTrigger:from AthenaCommon.AlgSequence import AlgSequence;AlgSequence().LVL1TGCTrigger.TILEMU=True;from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False\' \'ESDtoAOD:CILMergeAOD.removeItem("xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.LATERAL.LONGITUDINAL.SECOND_R.SECOND_LAMBDA.CENTER_MAG.CENTER_LAMBDA.FIRST_ENG_DENS.ENG_FRAC_MAX.ISOLATION.ENG_BAD_CELLS.N_BAD_CELLS.BADLARQ_FRAC.ENG_BAD_HV_CELLS.N_BAD_HV_CELLS.ENG_POS.SIGNIFICANCE.CELL_SIGNIFICANCE.CELL_SIG_SAMPLING.AVG_LAR_Q.AVG_TILE_Q.EM_PROBABILITY.PTD.BadChannelList");CILMergeAOD.add("xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.N_BAD_CELLS.ENG_BAD_CELLS.BADLARQ_FRAC.AVG_TILE_Q.AVG_LAR_Q.CENTER_MAG.ENG_POS.CENTER_LAMBDA.SECOND_LAMBDA.SECOND_R.ISOLATION.EM_PROBABILITY");StreamAOD.ItemList=CILMergeAOD()\' --postInclude \'default:RecJobTransforms/UseFrontier.py\' --preExec \'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(40.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(40);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True);\' \'HITtoRDO:userRunLumiOverride={"run":222525, "startmu":40.0, "endmu":41.0, "stepmu":1.0, "startlb":1, "timestamp": 1376703331}\' \'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.cutLevel.set_Value_and_Lock(14);\' \'ESDtoAOD:ToolSvc += CfgMgr.xAODMaker__TrackParticleCompressorTool( "xAODTrackParticleCompressorTool", OffDiagCovMatrixBits = 7 ); from JetRec import JetRecUtils;f=lambda s:["xAOD::JetContainer#AntiKt4%sJets"%(s,),"xAOD::JetAuxContainer#AntiKt4%sJetsAux."%(s,),"xAOD::EventShape#Kt4%sEventShape"%(s,),"xAOD::EventShapeAuxInfo#Kt4%sEventShapeAux."%(s,),"xAOD::EventShape#Kt4%sOriginEventShape"%(s,),"xAOD::EventShapeAuxInfo#Kt4%sOriginEventShapeAux."%(s,)]; JetRecUtils.retrieveAODList = lambda : f("EMPFlow")+f("LCTopo")+f("EMTopo")+["xAOD::EventShape#NeutralParticleFlowIsoCentralEventShape","xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoCentralEventShapeAux.", "xAOD::EventShape#NeutralParticleFlowIsoForwardEventShape","xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoForwardEventShapeAux.","xAOD::EventShape#ParticleFlowIsoCentralEventShape","xAOD::EventShapeAuxInfo#ParticleFlowIsoCentralEventShapeAux.", "xAOD::EventShape#ParticleFlowIsoForwardEventShape","xAOD::EventShapeAuxInfo#ParticleFlowIsoForwardEventShapeAux.", "xAOD::EventShape#TopoClusterIsoCentralEventShape","xAOD::EventShapeAuxInfo#TopoClusterIsoCentralEventShapeAux.", "xAOD::EventShape#TopoClusterIsoForwardEventShape","xAOD::EventShapeAuxInfo#TopoClusterIsoForwardEventShapeAux.","xAOD::CaloClusterContainer#EMOriginTopoClusters","xAOD::ShallowAuxContainer#EMOriginTopoClustersAux.","xAOD::CaloClusterContainer#LCOriginTopoClusters","xAOD::ShallowAuxContainer#LCOriginTopoClustersAux."]; from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.btaggingAODList=["xAOD::BTaggingContainer#BTagging_AntiKt4EMTopo","xAOD::BTaggingAuxContainer#BTagging_AntiKt4EMTopoAux.","xAOD::BTagVertexContainer#BTagging_AntiKt4EMTopoJFVtx","xAOD::BTagVertexAuxContainer#BTagging_AntiKt4EMTopoJFVtxAux.","xAOD::VertexContainer#BTagging_AntiKt4EMTopoSecVtx","xAOD::VertexAuxContainer#BTagging_AntiKt4EMTopoSecVtxAux.-vxTrackAtVertex"]; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(True);  AODFlags.ThinNegativeEnergyCaloClusters.set_Value_and_Lock(True); AODFlags.ThinNegativeEnergyNeutralPFOs.set_Value_and_Lock(True); from eflowRec.eflowRecFlags import jobproperties; jobproperties.eflowRecFlags.useAODReductionClusterMomentList.set_Value_and_Lock(True); from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");\'  --numberOfCavernBkg \'0\' --preInclude \'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py\'  --mts ESD:0 --steering "RAWtoESD:in-RDO,in+RDO_TRIG,in-BS" --ignoreErrors \'True\' '+amp+' '
    return (infile,f),'%s --inputHITSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,preExec)

def __hitstoaod_mc_ttbar(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/mc15_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e3698_s2726/HITS.06950958._003212.pool.root.1'
    return __Reco_tf_hitstoaod(infile,__nevts(nevts,builddate),short,'')

def __hitstoaod_mc_ttbar_mp4(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/mc15_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e3698_s2726/HITS.06950958._003212.pool.root.1'
    return __Reco_tf_hitstoaod(infile,__nevts(nevts,builddate),short,'--athenaopts \' --nprocs=4\'')

# default
def fullchain_mc15_ttbar_valid_13tev_25ns_mu40(**kw): return __hitstoaod_mc_ttbar(nevts=100,**kw)
def fullchain_mc15_ttbar_valid_13tev_25ns_mu40_mp4(**kw): return __hitstoaod_mc_ttbar_mp4(nevts=100,**kw)

#### system

def __Reco_tf_rdotoaod_system(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doTrigger=False;rec.doForwardDet=False;rec.doInDet=True;rec.doMuon=True;rec.doCalo=True;rec.doEgamma=False;rec.doMuonCombined=False;rec.doJetMissingETTag=False;rec.doTau=False;from RecExConfig.RecAlgsFlags import recAlgs;recAlgs.doMuonSpShower=False;rec.doBTagging=False;recAlgs.doEFlow=False;recAlgs.doEFlowJet=False;recAlgs.doMissingET=False;recAlgs.doMissingETSig=False; from JetRec.JetRecFlags import jetFlags;jetFlags.Enabled=False;\'  --ignoreErrors \'True\' --conditionsTag=OFLCOND-RUN12-SDR-25'
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)

def __Reco_tf_rdotoaod_combined(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doMonitoring=False;rec.doTrigger=False;\' --conditionsTag=OFLCOND-RUN12-SDR-25 --ignoreErrors \'True\' '
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)

def __Reco_tf_rdotoaod_monitoring(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doMonitoring=True;rec.doTrigger=False;\' --conditionsTag=OFLCOND-RUN12-SDR-25 --ignoreErrors \'True\' '
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)


def __Reco_tf_rdotoaod_trigger(infile,nevts,short):
    if short: nevts=1
    f=infile
    setup=' --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --preExec=\'from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True;rec.doMonitoring=True;rec.doTrigger=True;\' --conditionsTag=OFLCOND-RUN12-SDR-25 --ignoreErrors \'True\' '
    return (infile,f),'%s --inputRDOFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,setup)


def __rdotoaod_mc_ttbar_system(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_system(infile,__nevts(nevts,builddate),short)

def __rdotoaod_mc_ttbar_combined(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_combined(infile,__nevts(nevts,builddate),short)

def __rdotoaod_mc_ttbar_monitoring(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_monitoring(infile,__nevts(nevts,builddate),short)

def __rdotoaod_mc_ttbar_trigger(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/mc/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO.e3099_s2578_r6220_tid05191878_00/RDO.05191878._000121.pool.root.1'
    return __Reco_tf_rdotoaod_trigger(infile,__nevts(nevts,builddate),short)

# default
def system_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_system(nevts=100,**kw)
def combined_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_combined(nevts=100,**kw)
def monitoring_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_monitoring(nevts=100,**kw)
def trigger_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40(**kw): return __rdotoaod_mc_ttbar_trigger(nevts=100,**kw)

####



def __Reco_tf_rawtoall(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg=' --autoConfiguration \'everything\' --conditionsTag  \'CONDBR2-BLKPA-2016-16\' --geometryVersion  \'ATLAS-R2-2015-04-00-00\'  --outputHISTFile  \'myHIST.pool.root\' --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputDRAW_TAUMUHFile  \'myDRAW_TAUMUH.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --athenaopts=\' --pmon=sdmonfp\' --ignoreErrors \'True\' --steering doRAWtoALL'
    return (infile,f),'%s --inputBSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)


def __Reco_tf_rawtoall_mp4(infile,nevts,short):
    if short: nevts=1
    f=infile
    cfg=' --autoConfiguration \'everything\' --conditionsTag  \'CONDBR2-BLKPA-2016-16\' --geometryVersion  \'ATLAS-R2-2015-04-00-00\'  --outputHISTFile  \'myHIST.pool.root\' --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputDRAW_TAUMUHFile  \'myDRAW_TAUMUH.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --athenaopts=\' --pmon=sdmonfp\' --ignoreErrors \'True\' --steering doRAWtoALL  --athenaopts \' --nprocs=4\'  '

    return (infile,f),'%s --inputBSFile %s --maxEvents %i %s'%(Reco_tf_cmd(),f,nevts,cfg)

def __rawtoall_data16(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/data16_13TeV/data16_13TeV.00305777.physics_Main.daq.RAW._lb0290._SFO-6._0002.data'
    return __Reco_tf_rawtoall(infile,__nevts(nevts,builddate),short)

def __rawtoall_data16_mp4(branch,cmtcfg,builddate,short,nevts):
    infile='/build1/atlaspmb/data16_13TeV/data16_13TeV.00305777.physics_Main.daq.RAW._lb0290._SFO-6._0002.data'
    return __Reco_tf_rawtoall_mp4(infile,__nevts(nevts,builddate),short)

# default
def rawtoall_tier0_reco_data16(**kw): return __rawtoall_data16(nevts=500,**kw)
def rawtoall_tier0_reco_data16_mp4(**kw): return __rawtoall_data16_mp4(nevts=-1,**kw)


##########################################################################################################################################################

joblist_default = [
    fullchain_mc15_ttbar_valid_13tev_25ns_mu40,
    rawtoall_tier0_reco_data16,
#    fullchain_mc15_ttbar_valid_13tev_25ns_mu40_mp4,
#    rawtoall_tier0_reco_data16_mp4,
    system_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40,
    combined_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40,
#    monitoring_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40,
#    trigger_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40
    ]

#############################################################################

runlist=[]

if os.getenv('HOSTNAME')=='aibuild010.cern.ch':
    runlist+=[('21.0','x86_64-slc6-gcc62-opt',joblist_default)]
    runlist+=[('master','x86_64-slc6-gcc62-opt',joblist_default)]
    
#############################################################################


def get_load():
    #duplicated in Misc.py
    p=subprocess.Popen('/bin/ps -eo pcpu --no-headers'.split(), stdout=subprocess.PIPE).communicate()[0]
    return 0.01*sum(map(lambda x:float(x),p.split()))


def get_n_cores():
    #duplicated in Misc.py
    n=0
    for l in open('/proc/cpuinfo'):
        if l.startswith('processor') and l.split()[0:2]==['processor',':']:
            n+=1
    return n


def ok_to_run():
    l=get_load()
    n=get_n_cores()
    if n==1: return l<0.05
    return l<n*0.33


def memory_status(username):
    used_memory = subprocess.Popen("ps -u %s -o rss | awk '{sum+=$1} END {print sum}'" % username,shell=True,stdout=subprocess.PIPE).communicate()[0]                           
    return int(used_memory)


if not ok_to_run():
    sys.exit(0)#not an error, will relaunch later


if memory_status('atlaspmb') > 15000000:
    print 'not enough memory available. exit!'
    sys.exit(0)


##############################################################################
######################### ABORT IF LIMITED SPACE #############################
##############################################################################


def get_free_space(mountpoint):
    if not os.path.exists(mountpoint):
        return None
    for l in subprocess.Popen(['df','-k',mountpoint], stdout=subprocess.PIPE).communicate()[0].split('\n'):
        l=l.split()
        if l and l[-1]==mountpoint: return float(l[-3])/(1024.0*1024.0)

space_ok=True
for mountpoint,limit in free_space_checks_gb:
    fs=get_free_space(mountpoint)
    if fs==None:
        print "Could not determine space left on %s"%mountpoint
        space_ok=False
    elif fs<limit:
        print "Too little space left on %s : %.2f gb (required %.2f gb)"%(mountpoint,fs,limit)
        space_ok=False
if not space_ok:
    print "Aborting."
    sys.exit(1)


##############################################################################
#################### FIGURE OUT WHICH JOBS TO RUN ############################
##############################################################################


def touch(filename):
    #only creates if doesn't exist. Won't update timestamp of existing file.
    if not os.path.exists(filename):
        open(filename,'wa').close()

def decode_build_date(relNstr,modtime):
    assert relNstr.startswith('rel_') and len(relNstr)==5 and relNstr[-1] in '0123456'
    weekday=int(relNstr[4:])
    weekday=(weekday-1)%7#to get monday=0, sunday=6
    assert weekday>=0 and weekday<=6
    builddate=datetime.date.fromtimestamp(modtime)
    #Adjust builddate so it matches the rel_X number exactly (rel_1=monday, etc.):
    if builddate.weekday()!=weekday:
        #print "WARNING: Correcting date!!!"
        #print weekday,'  ',builddate.weekday()
        deltadays=weekday-builddate.weekday()
        if deltadays>=5: deltadays-=7
        if deltadays<=-5: deltadays+=7
        #print deltadays
        #assert deltadays>=-2 and deltadays<=2
        builddate=builddate+datetime.timedelta(deltadays)
    return builddate


#asetup='export AtlasSetup=%s && source %s/scripts/asetup.sh'%((asetup_dir,)*2)
asetup='source %s/scripts/asetup.sh'%(asetup_dir)

for b,cmtcfg,joblist in runlist:

    #Define the ATLAS project according to the release
    atlas_project = 'Athena'

    pattern='%s/%s/*/%s/*/InstallArea/%s/bin'%(nightlies_dir_cvmfs,b,atlas_project,cmtcfg)
    
    print b,cmtcfg,joblist
    print pattern
    dirs=sorted(glob.glob(pattern))
    
    for d in dirs:
        print d
        if not os.path.isdir(d): continue#protect against afs issues
        dtime=os.path.getmtime(d)

        hours_ago=(time.time()-dtime)/(3600.0)
        #To stay away from builds in progress the timestamp should have an age of at least 3 hours
        if hours_ago<3:
            print 'hours_ago<3'
            continue
        #To stay away from builds about to be restarted, the timestamp should have an age of at most 5 days.
        if hours_ago>24*6:
            print 'hours_ago>24*5'
            continue
        #Get build date:
        relN=d.split('/')[-6]

        relsplit=relN.split('-')
        if (len(relsplit) != 3): continue
        year=relsplit[0]
        month=relsplit[1]
        day=relsplit[2].split('T')[0]
        builddate=datetime.date(int(year), int(month), int(day))

        build_archive_dir = os.path.join(archive_dir,builddate.strftime('%d/%m/%Y'),b,cmtcfg)
        #Figure out atlas setup command:
        asetup_args=cmtcfg.split('-')+[b,relN]
        if asetup_args[0]=='x86_64': asetup_args[0]='64'
        else: asetup_args[0]='32'
        asetup_cmd=''
        asetup_cmd=asetup+' '+','.join(asetup_args)

        asetup_cmd += ','+atlas_project
        

#if b == '17.1.X.Y-VAL': asetup_cmd += ',AtlasP1HLT'



        #elif b=='17.3.X.Y.Z-VAL' or b=='17.2.X.Y.Z-VAL' : asetup_cmd += ',AtlasPhysics'

        #if 'dev_perf_test' == runlist[0][2].__name__:
        #    asetup_cmd += ' --testarea=/afs/cern.ch/atlas/project/pmb/new_pmb/software/testarea/dev_perf_test'+runlist[0][0]
        #else:
        #    
        

        #Figure out which jobs to run:
        for job in joblist:
            print job.__name__
            #if 'dev_perf_test' in job.__name__:
            #    asetup_cmd += ' --testarea=/afs/cern.ch/atlas/project/pmb/new_pmb/software/testarea/dev_perf_test_1'
                #asetup_cmd += ' --testarea=/afs/cern.ch/atlas/project/pmb/new_pmb/software/testarea/dev_perf_test'
            #else:
            asetup_cmd += ' --testarea=/afs/cern.ch/atlas/project/pmb/new_pmb/software/testarea/'+runlist[0][0]   

#Get command + short command:
            infiles,cmd=job(branch=b,cmtcfg=cmtcfg,builddate=builddate,short=False) #old

            #infiles,cmd=job
            if not cmd: continue#not on this build apparently
            '''
            for srcfile,localfile in infiles:
                if os.path.exists(localfile): continue
                if not os.path.isdir(os.path.dirname(localfile)):
                    os.makedirs(os.path.dirname(localfile))
                #print srcfile
                #print localfile
                if 'root://eosatlas///' in srcfile:
                    eos_copy_cmd='xrdcp '+srcfile+' '+localfile
                    os.system(eos_copy_cmd)
                else: 
                    shutil.copy2(srcfile,localfile)
                assert os.path.exists(localfile)
            '''
            infiles_short,cmd_short=job(branch=b,cmtcfg=cmtcfg,builddate=builddate,short=True)
            assert infiles==infiles_short
            assert cmd_short
            
            #Check if done already done, and if not acquire "lock" by making target output dir:
            target_dir=os.path.join(build_archive_dir,job.__name__)
            file_done=os.path.join(target_dir,'__done')
            file_start=os.path.join(target_dir,'__start')

            #file_exitcode=os.path.join(target_dir,'__exitcode')
            #file_logfile=os.path.join(target_dir,'__log.txt.gz')
            if os.path.isdir(target_dir):
                if os.path.exists(file_done):
                    continue
                #already running or done (todo: check here that the job actually finished in 24 hours... otherwise delete and relaunch)
                if not os.path.exists(file_start):
                    time.sleep(60)
                if not os.path.exists(file_start) or (time.time()-os.path.getmtime(file_start))>24*3600:
                    print "Warning: Having to remove %s"%target_dir
                    shutil.rmtree(target_dir)
                else:
                    continue

            try:
                os.makedirs(target_dir)#Now we are in charge...
                assert os.path.isdir(target_dir)
                touch(file_start)
            except Exception,err:
                print "ERROR: Problems encountered while making %s"%target_dir
                print "Aborting."
                if os.path.isdir(target_dir): shutil.rmtree(target_dir)
                sys.exit(1)
                pass

            #Temporary run-dir:
            tmpdir=os.path.join(tmpdir_base,'__'.join([b,cmtcfg,relN,job.__name__]))
            tmpdir_start=os.path.join(tmpdir,'__start')
            if os.path.exists(tmpdir):
                canremove=False
                if not os.path.exists(tmpdir_start): time.sleep(20)
                if not os.path.exists(tmpdir_start): canremove=True
                elif (time.time()-os.path.getmtime(tmpdir_start))>24*3600: canremove=True
                if not canremove:
                    print "WARNING: Found unexpected recent tmp rundir %s"%tmpdir
                    continue
                shutil.rmtree(tmpdir)
            os.makedirs(tmpdir)
            touch(tmpdir_start)
            #NB: Stop after one job has run! Cron will relaunch us each hour and will then run the next job!
            def cmd_gen(_cmd,_asetup_cmd,rundir,infiles,relN):
                file_command=os.path.join(rundir,'__command.sh')
                cmds=['#!/bin/bash',
                      '','#Input files are copied locally before running. Sources are:']
                i=0
                '''
                for src,local in infiles:
                    i+=1
                    cmds+=['# %i) %s => %s'%(i,src,local)]
                '''
                cmds+=['export TRF_ECHO=1;']
                cmds+=['','touch __start_asetup',_asetup_cmd,'touch __start',_cmd+' >__log.txt 2>&1','echo $? > __exitcode']

                buildbranch=os.environ['AtlasBuildBranch']
                buildstamp=os.environ['AtlasBuildStamp']
                platform=os.environ['Athena_PLATFORM']
                                        
                relsplit=buildstamp.split('-')
                year=relsplit[0]
                month=relsplit[1]
                day=relsplit[2].split('T')[0]
                builddate=datetime.date(int(year), int(month), int(day))
                dayofweek=builddate.weekday()-1
                relN
                cmds+=['perl -pi -e "s/private\/private/${AtlasBuildBranch}\/rel'+str(dayofweek)+'/g;" __log.txt']
                cmds+=['for f in my*.pool.root; do',
                       '    if [ -f "$f" ]; then checkxAOD.py $f > $f.checkfile.txt 2>/dev/null; fi',
                       'done']
                cmds+=['BAD=0',
                       'for f in ntuple*.pmon.gz; do',
                       '    if [ -f "$f" ]; then tar xf $f "*.pmonsd.*" || BAD=1; fi',
                       'done',
                       'if [ $BAD != 0 ]; then echo "ERROR: tar problems in $PWD"; fi']
                cmds+=["ls -oqAQ1S --block-size=1 --ignore='__*' > __dirlist.txt"]

                cmds+=['rm -f *.pool.root']
                cmds+=['#Python code to create sqlite file']
                cmds+=['export PYTHONPATH=${BASEDIR}/python:$PYTHONPATH']
                cmds+=['python << END']
                cmds+=['']
                cmds+=['import PmbUtils.pmbDB as mydb']
                cmds+=['import os']
                cmds+=['x=mydb.pmbDB(os.path.dirname(os.path.realpath(\'__exitcode\')).replace(\'/\',\'__\')+\'.db\')']
                cmds+=['jobname=\''+job.__name__+'\'']
                cmds+=['release=os.environ[\'AtlasBuildBranch\']']
                cmds+=['platform=os.environ[\'Athena_PLATFORM\']']
                cmds+=['nightly=\''+relN+'\'']
                cmds+=['x.addEntry(release,platform,nightly,jobname,\'./\')']
                cmds+=['']
                cmds+=['END']
                cmds+=['rsync *.db /build/atlaspmb/custom_nightly_tests/database_staging_area/.']
                cmds+=['gzip *.txt','touch __done','']

                fh=open(file_command,'w')
                fh.write('\n'.join(cmds))
                fh.close()
                return file_command

            tmpdir_shortrun=os.path.join(tmpdir,'prerun')
            os.makedirs(tmpdir_shortrun)
            tmpdir_run=os.path.join(tmpdir,'run')
            os.makedirs(tmpdir_run)
#            cmdfile_short=cmd_gen(cmd_short,asetup_cmd,tmpdir_shortrun,infiles_short,relN)
            cmdfile=cmd_gen(cmd,asetup_cmd,tmpdir_run,infiles,relN)
#            ec1=os.system('cd %s && source %s'%(tmpdir_shortrun,cmdfile_short))
            ec2=os.system('cd %s && source %s'%(tmpdir_run,cmdfile))
            if ec2:
                print "NB: Problems in job",target_dir
            collect=[]
            for f in os.listdir(tmpdir_run):
                if os.path.isdir(f): continue
                bn=os.path.basename(f)
                if bn.startswith('__') or '.db' in bn or '.checkfile.txt.gz' in bn or 'pmonsd' in bn or 'ntuple' in bn or bn.startswith('log') or bn.startswith('mem') or 'jobReport.json' in bn:
                    collect+=[bn]
            for c in collect:
                shutil.copy2(os.path.join(tmpdir_run,c),target_dir)
            shutil.rmtree(tmpdir)
### here you can copy the db files to the staging area ### or rather merge them into the master file

            touch(file_done)
            sys.exit(0)#On purpose we stop after one job has run...

#colour weekends differently in plots? Vertical red lines when input file changed



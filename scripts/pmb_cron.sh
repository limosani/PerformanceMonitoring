#!/bin/bash
#### What does this script do?
source ~/.zshrc
source /afs/cern.ch/atlas/project/pmb/new_pmb/setup_scripts/setup.sh
asetup_dir='/afs/cern.ch/atlas/software/dist/AtlasSetup'
yesterday=`date -d yesterday +"%e"`
yesterday=28
source ${asetup_dir}/scripts/asetup.sh  master,r${yesterday},here,Athena


export TRF_ECHO=True
SERVICE=pmb_cron.py
python=python
counts=`ps ax | grep -v grep | grep $python | grep $SERVICE | wc -l`
max_counts=8
if [ "$counts" -ge "$max_counts" ]
then
    exit 0
else
    python /afs/cern.ch/atlas/project/pmb/new_pmb/bin/pmb_cron.py
fi
wait


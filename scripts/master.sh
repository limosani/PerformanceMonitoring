#!/bin/bash

source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh master,latest,Athena
export PYTHONPATH=${BASEDIR}/python:$PYTHONPATH

#
python << END
from os import path
from sqlalchemy import *
import PmbUtils.pmbDB as mydb
import glob
import time
import os


x=mydb.pmbDB('/build/atlaspmb/custom_nightly_tests/database_staging_area/master_0.db')

filelist=glob.glob('/build/atlaspmb/custom_nightly_tests/database_staging_area/__b*db')

current_time = time.time()

for file in filelist[:]:
    creation_time = path.getctime(file)
    if (current_time - creation_time) > 3600000 :     
        filelist.remove(file)
    
x.merge_db_files(filelist)    


for file in filelist:
    os.remove(file)

END


rsync -auvz /build/atlaspmb/custom_nightly_tests/database_staging_area/master_0.db /eos/atlas/user/a/atlaspmb/archive/custom/.
rsync -auvz /eos/atlas/user/a/atlaspmb/archive/custom/master_0.db .


python << END
from os import path
from sqlalchemy import *
import PmbUtils.pmbDB as mydb
import PmbUtils.pmbWebPage as myweb
import glob
import time
import sys
import os
import subprocess

dbName='./master_0.db'
x=mydb.pmbDB(dbName)

releases=['21.0','master']

platforms=['x86_64-slc6-gcc62-opt']
jobs=['rawtoall_tier0_reco_data16', \
      'fullchain_mc15_ttbar_valid_13tev_25ns_mu40', \
      'system_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40', \
      'combined_reco_mc15_ttbar_valid_13tev_25ns_mu00_to_mu40']


jobdict={}
for jobname in jobs:
    jobdict[jobname]=myweb.PMBWebPage(dbName,jobname,releases,platforms)
    if jobdict[jobname].steps == [] : 
       print jobname, " has no steps to process" 
       continue
    else:
       print "processing ",jobname

    jobdict[jobname].Build_All_WebPages()

    if len(releases)>1 or len(platforms)>1:
        for rel in releases:
            for plat in platforms:
                jobdict[jobname+str(rel)+str(plat)]=myweb.PMBWebPage(dbName,jobname,[rel],[plat])
                jobdict[jobname+str(rel)+str(plat)].Build_All_WebPages()

for jobname in jobs:
    cmd = 'rsync -avzu  arch-mon-'+jobname+'/* /eos/project/a/atlasweb/www/atlas-pmb/arch-mon-'+jobname+'/. '
    output,error = subprocess.Popen(['/bin/bash', '-c', cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    print cmd
    print jobname
    print output
    print error
END


